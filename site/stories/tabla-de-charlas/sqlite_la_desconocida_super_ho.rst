.. title: SQLite, la (des)conocida Super Hormiga
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias
.. category: stories
.. link:
.. description:
.. type: text



Abstract
============
¿Cómo extraer todo el jugo a esta conocida base de datos?

Descripción
===============
Empiezo por explicar las ventajas y usos específicos de este tipo de base de datos, mucho más apropiado para ciertos usos que las conocidas 
bases de datos multiusuario. Prosigo enseñando algunas funciones LAMBDA, y como hacer tipos especializados de datos, ordenamientos especializados, etc. 
Cierro la charla mostrando algunas de las opciones menos conocidas de esta tecnología, como el uso de indexado para la búsqueda por texto completo.

Contenido
=========
- SQLite y Python: mejores prácticas
- Transacciones, Aislamiento y Concurrencia
- Indices
- Tipos de datos especiales
- Automatizar y extender

Recursos
=============
🎞 : `Video PyConAr 2020:1 <https://youtu.be/QPgt4QpcfRs>`_

✍ :  `Presentación PyConAr_2020:1 <https://eventos.python.org.ar/events/pyconar2020/activity/394/>`_

🎞 :  `Video PyConEs 2021 <https://youtu.be/t2yEmbPQlY4>`_

💾: `Slides </SqliteAvanzado.pdf>`_

.. title: Haz ORO tus datos con SQLAlchemy
.. date: 2022-02-07 23:22:34 UTC+03:00
.. tags: charlas historial refencias python sqlalchemy
.. category: stories
.. link:
.. description:
.. type: text



Abstract
============
Introducción y cómo usar el Object Relational Mapper más maduro. Mostraré sus ventajas y la facilidad para hacer estructuras complejas y rápidamente manipularlas.


Descripción
=============
Muestro el uso de esta biblioteca cuyo uso está  muy extendido. 
Es un recorrido que nos permite tener un pantallazo de su profundidad, y comprenderla con poco esfuerzo.


Contenido
=========
* Porqué razones es conveniente usar un ORM en vez de las consultas directas. 
* Cómo declarar tablas, columnas, relaciones desde nuestro código con alto nivel. 
* Altas, modificaciones, y bajas hechas con toda sencillez. 
* Las consultas y un lenguaje de más alto nivel que nos permite construirlas con gran facilidad.
* Como podemos manejar bases de datos existentes, migraciones e integrar con Flask.


Recursos
=============
✍ : `Presentation PyCon NEA 2022 <https://pycon-archive.python.org/2019/schedule/presentation/342/>`_

💾: `Diapositivas SQLAlchemy </Alchemy.pdf>`_

🎞 : Video no disponible

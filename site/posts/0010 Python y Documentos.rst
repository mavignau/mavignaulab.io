.. title: Python y documentos
.. date: 2022-02-08 19:22:34 UTC-03:00
.. tags: charla, python, pdf, documentos
.. category: blog
.. link:
.. description:
.. type: text



==================================
Python y Documentos
==================================

Links a las bibliotecas mencionadas
-------------------------------------

- `weasyprint <https://doc.courtbouillon.org/weasyprint/stable/index.html>`_
- `xhtml2pdf <https://xhtml2pdf.readthedocs.io/en/latest/index.html>`_
- `rinohtype <http://www.mos6581.org/rinohtype/master>`_
- `secretary <https://github.com/christopher-ramirez/secretary>`_
- `rst2pdf <https://rst2pdf.org>`_
- `pdfminer.six <https://pdfminersix.readthedocs.io/en/latest/index.html>`_
- `Camelot <https://camelot-py.readthedocs.io/en/master/index.html>`_
- `PyMuPDF <https://pymupdf.readthedocs.io/en/latest/index.html>`_
- `PyPDF4 <https://pythonhosted.org/PyPDF2>`_

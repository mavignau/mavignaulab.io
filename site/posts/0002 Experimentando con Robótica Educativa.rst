.. title: Experimentando con Robótica Educativa
.. date: 2018-07-28 23:22:34 UTC+02:00
.. tags: robótica, educativo, rodi, rodibot
.. category: blog
.. link:
.. description:
.. type: text

Este año vino al PyDay NEA a dar su charla Gary Servín @garyservin, que en Paraguay empezó
un proyecto de robótica educativa llamado `RoDI <http://rodibot.com/#>`_, acompañado por  Mau Gavilán
@mauroot.

Los había conocido en la primera edición de Robotec, año 2017, que se hizo
en la Casa de las Culturas en Resistencia, Chaco, donde fuí con mis hijas.

Este año pude ir con mi hija menor, de 11 años, para que ella empiece a tomarle el
gustito a ésto de la informática, la robótica y se divierta. Jugando
con RoDI pasamos un excelente rato en Corrientes.

En la segunda edición de Robotec, decidí comprarle uno de los robotitos,
así nos divertíamos un poco. Nos divertimos las dos un buen rato,
programamos un poco usando la librería de python creada por Manuel Kaufmann @reydelhumo.

El código fuente está en `rodi-fun <https://gitlab.com/mavignau/rodi-fun>`_

Finalmente, nos divertimos filmando un video que muestra las habilidades del pequeño robot.

{{% media url="https://www.youtube.com/watch?v=kUzuwhCGZ8o" %}}


.. title: Introducción a Python con Jupyter
.. date: 2022-09-29 9:22:34 UTC-03:00
.. tags: charla, matematica, ingeniería, jupyter
.. category: blog
.. link:
.. description:
.. type: text

Tuve el placer de dar charla para las primeras jornadas de Educación de Matemáticas en ingeniería

.. figure:: /galleries/CongresoMatematicas/CongresoMatematicasUNNE2022-02.jpeg
   :width: 1200px
   :scale: 50 %
   :align: center
   :figwidth: 80%
   :alt: Dando charla en la Facultad de Ingeniería

Más información
~~~~~~~~~~~~~~~~~~

`Galería de fotos </galleries/CongresoMatematicas/>`_

`Resúmen de la charla, enlaces, diapositivas y contenido </stories/tabla-de-charlas/introduccin_a_python_con_jupyt/>`_
